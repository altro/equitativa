# Equitativa

For start application:
1. Install JDK11, maven, PostgreSQL
2. Create database **tests** and user **tests**. For this run script `src/main/resources/db/create/create_db.sql`

   **IMPORTANT**
    <p>Because user without password in postgresql is not standard behaviour, 
   please make sure that the password is correct (in the example, the user password is <b>1</b>).

4. Run `mvn clean flyway:migrate jetty:run`
5. Open web app in your browser: `http://localhost:8080`
6. Sign up new user with role `USER`
