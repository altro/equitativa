package com.equitativa.testwork.util;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class StringUtilTest {

    @Test
    void test() {
        String str = "test123";

        var toHex = StringUtil.toHex(str.getBytes());

        var fromHex = new String(StringUtil.hexToArray(toHex));

        assertEquals(str, fromHex);
    }

}