package com.equitativa.testwork.dao;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.model.Status;
import com.equitativa.testwork.model.User;
import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.util.StringUtil;

import static com.equitativa.testwork.util.StringUtil.getSalt;
import static com.equitativa.testwork.util.StringUtil.toHex;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Disabled
@Slf4j
class UserDaoTest {

    HibernateContext hibernateContext = HibernateContext.get();
    UserDao userDao = UserDao.get();

    @Test
    void crudTest() {

        var password = "Passw0rd";
        var login = "testLogin";

        var salt = getSalt();
        var encodedPassword = StringUtil.sha256(password, salt);
        var now = LocalDateTime.now();

        var user = User.builder()
                .status(Status.ACTIVE)
                .login(login)
                .password(encodedPassword)
                .salt(toHex(salt))
                .createTime(now)
                .updateTime(now)
                .role(UserRole.USER)
                .build();

        var userId = userDao.save(user);
        user.setId(userId);

        log.info("User {}:{} created", login, userId);

        user.setEmail("aaaa@sddddd.com");
        user.setName("Mister test");

        userDao.save(user);
        log.info("User {}:{} updated", login, userId);

        var foundUser = userDao.get(User.class, userId);
        log.info("Found user {}:{}", foundUser.getLogin(), foundUser.getId());
        assertEquals(foundUser.getLogin(), user.getLogin());
        assertEquals(foundUser.getEmail(), user.getEmail());
        assertEquals(foundUser.getPassword(), user.getPassword());

        userDao.delete(foundUser);
        log.info("User {}:{} deleted", foundUser.getLogin(), foundUser.getId());

        foundUser = userDao.get(User.class, userId);
        assertNull(foundUser);
        log.info("User {} not found", userId);
    }

}