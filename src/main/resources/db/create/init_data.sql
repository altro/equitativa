insert into buildings (code, title)
values ('b1', 'Building One'),
       ('b2', 'Building Two'),
       ('b3', 'Building Three');

insert into project_types (code, title)
values ('t1', 'Type 1'),
       ('t2', 'Type 2'),
       ('t3', 'Type 3'),
       ('t4', 'Type 4');

insert into users (role, login, password, salt, name, email)
values ('w', 'worker1', '', '', 'John', 'john@sddfdf.com'),
       ('w', 'worker2', '', '', 'Igor', 'igor@sddfdf.com'),
       ('w', 'worker3', '', '', 'Liam', 'liam@sddfdf.com');

insert into projects (state, type_id, building_id, assigned_user_id, start_time, finish_time)
values ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b1'), null, null, null),
       ('NEW', (select id from project_types where code = 't2'), (select id from buildings where code = 'b1'), null, null, null),
       ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b1'), null, null, null),
       ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b1'), null, null, null),
       ('COMPLETED', (select id from project_types where code = 't3'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker1'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('COMPLETED', (select id from project_types where code = 't3'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker2'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('COMPLETED', (select id from project_types where code = 't4'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker3'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('COMPLETED', (select id from project_types where code = 't1'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker1'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('IN_PROGRESS', (select id from project_types where code = 't2'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker2'), '2021-07-01 00:04:00', null),
       ('IN_PROGRESS', (select id from project_types where code = 't4'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker1'), '2021-07-10 00:04:00', null),
       ('ASSIGNED', (select id from project_types where code = 't4'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker1'), '2021-07-10 00:04:00', null),
       ('ASSIGNED', (select id from project_types where code = 't4'), (select id from buildings where code = 'b1'),
        (select id from users where login = 'worker1'), '2021-07-10 00:04:00', null),

       ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b2'), null, null, null),
       ('NEW', (select id from project_types where code = 't3'), (select id from buildings where code = 'b2'), null, null, null),
       ('COMPLETED', (select id from project_types where code = 't4'), (select id from buildings where code = 'b2'),
        (select id from users where login = 'worker1'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('COMPLETED', (select id from project_types where code = 't4'), (select id from buildings where code = 'b2'),
        (select id from users where login = 'worker2'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),
       ('COMPLETED', (select id from project_types where code = 't1'), (select id from buildings where code = 'b2'),
        (select id from users where login = 'worker3'), '2021-06-01 00:04:00', '2021-06-10 10:04:00'),

       ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't2'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't3'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't4'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't4'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't2'), (select id from buildings where code = 'b3'), null, null, null),
       ('NEW', (select id from project_types where code = 't1'), (select id from buildings where code = 'b3'), null, null, null)
;