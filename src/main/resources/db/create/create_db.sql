SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'tests'
  AND pid <> pg_backend_pid();

drop database if exists tests;

DROP ROLE IF EXISTS tests;

CREATE ROLE tests WITH LOGIN
    PASSWORD '1'
    SUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

CREATE DATABASE tests
    WITH OWNER = tests
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
