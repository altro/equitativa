create table if not exists users
(
    id bigserial primary key,
    status char(1) not null default 'A',
    role char(1) not null default 'u',
    login varchar(255) not null unique,
    password varchar(256) not null,
    salt varchar(50) not null,
    name varchar(255),
    email varchar(255),
    create_time timestamp without time zone not null default now(),
    update_time timestamp without time zone not null default now(),
    last_login_time timestamp without time zone
);

create table if not exists buildings
(
    id bigserial primary key,
    status char(1) not null default 'A',
    code varchar(255) not null unique,
    title varchar(255) not null
);

create table if not exists project_types
(
    id bigserial primary key,
    status char(1) not null default 'A',
    code varchar(255) not null unique,
    title varchar(255) not null
);

create table if not exists projects
(
    id bigserial primary key,
    status char(1) not null default 'A',
    state varchar(25) not null,
    type_id bigint not null references project_types,
    building_id bigint not null references buildings,
    assigned_user_id bigint references users,
    start_time timestamp without time zone,
    finish_time timestamp without time zone,

    create_time timestamp without time zone not null default now(),
    update_time timestamp without time zone not null default now()
);
