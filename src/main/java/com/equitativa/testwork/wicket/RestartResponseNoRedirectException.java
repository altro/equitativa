package com.equitativa.testwork.wicket;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.request.handler.RenderPageRequestHandler;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class RestartResponseNoRedirectException extends RestartResponseException {

    private static final long serialVersionUID = 1L;

    public <C extends Page> RestartResponseNoRedirectException(final Class<C> pageClass) {
        this(pageClass, null);
    }

    public <C extends Page> RestartResponseNoRedirectException(final Class<C> pageClass, final PageParameters params) {
        super(new PageProvider(pageClass, params), RenderPageRequestHandler.RedirectPolicy.AUTO_REDIRECT);
    }
}
