package com.equitativa.testwork.wicket;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.core.request.handler.IPageRequestHandler;
import org.apache.wicket.core.request.mapper.StalePageException;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.component.IRequestablePage;
import org.apache.wicket.request.cycle.PageRequestHandlerTracker;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.cycle.RequestCycleContext;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.util.TimeUtil;

@Slf4j
public class TestWorkRequestCycle extends RequestCycle {

    private static final long PROCESS_WARNING_TIME = TimeUtil.SECOND_MILLIS * 5;

    public TestWorkRequestCycle(RequestCycleContext context) {
        super(context);
    }

    @Override
    protected void onEndRequest() {

        var session = TestWorkWebSession.getSession();
        Request request = getRequest();
        Url requestUrl = request.getUrl();

        try {
            long workTime = System.currentTimeMillis() - getStartTime();
            if (workTime > PROCESS_WARNING_TIME && !TestWorkApplication.justStarted()) {
                if (log.isWarnEnabled()) {
                    log.warn("Processing time TOO LONG: {} ms (User: {}, url: {})", workTime, session.getUserId(), requestUrl);
                }
            }

            HibernateContext.get().closeSession(true);

        } catch (StalePageException e) {
//                IRequestablePage page = ((StalePageException) e).getPage();
//                log.error("Exception: user: {}, page: {}: {}",
//                (session.isLoggedIn() ? session.getUserInfo() : session.getUserId()), page.getClass().getSimpleName(), page.getPageParameters(), e);
        } catch (RestartResponseNoRedirectException | RestartResponseAtInterceptPageException e) {
            HibernateContext.get().closeSession(true);
        } catch (RuntimeException e) {
            log.error("Exception occurred during onEndRequest, user: {}, page: {}", session.getUserId(), pageName(), e);
            HibernateContext.get().closeSession(true);
        }

/*
        if (!session.isValuable() && "true".equalsIgnoreCase(System.getProperty("config.invalidateSessions"))) {
            session.invalidateNow();
            log.debug("Session invalidated {}", requestUrl);
        }
*/
    }

    private String pageName() {
        IPageRequestHandler pageRequestHandler = PageRequestHandlerTracker.getLastHandler(RequestCycle.get());
        if (pageRequestHandler == null) {
            return "";
        }
        IRequestablePage page = pageRequestHandler.getPage().getPage();
        return page == null ? "" : page.getClass().getSimpleName();
    }

}
