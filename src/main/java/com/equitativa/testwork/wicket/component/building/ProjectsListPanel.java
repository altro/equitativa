package com.equitativa.testwork.wicket.component.building;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import com.equitativa.testwork.model.Project;
import com.equitativa.testwork.service.ProjectService;
import com.equitativa.testwork.wicket.model.PageableModel;

public class ProjectsListPanel extends Panel {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.uuuuu");

    private PageableModel<Project> projectsModel;

    public ProjectsListPanel(String id, IModel<Long> userIdModel, Long buildingId) {
        super(id, userIdModel);

        add(new ListView<>("projects", projectsModel = new PageableModel<>(15) {
            @Override
            protected int getElementsCount() {

                return ProjectService.get().countProjects(buildingId, (Long) getDefaultModelObject());
            }

            @Override
            protected List<Project> doLoad(int first, int count) {
                if (getTotalElements() == 0) {
                    return Collections.emptyList();
                }
                return ProjectService.get().findProjects(buildingId, (Long) getDefaultModelObject(), first, count);
            }
        }) {
            @Override
            protected void populateItem(ListItem<Project> listItem) {
                var project = listItem.getModelObject();

                listItem.add(new Label("projectId", project.getId()))
                        .add(new Label("type", project.getType().getTitle()))
                        .add(new Label("createTime", DATE_FORMAT.format(project.getCreateTime())))
                ;

                if (project.getAssignedUserId() != null) {
                    listItem.add(new Label("assignedUser", project.getAssignedUser().getName()));
                } else {
                    listItem.add(new WebMarkupContainer("assignedUser"));
                }

                String stateCss;
                switch (project.getState()) {
                    case ASSIGNED:
                        stateCss = "bg-dark";
                        break;
                    case IN_PROGRESS:
                        stateCss = "bg-info text-dark";
                        break;
                    case COMPLETED:
                        stateCss = "bg-success";
                        break;
                    case NEW:
                    default:
                        stateCss = "bg-primary";
                        break;
                }
                listItem.add(new Label("state", project.getState().name())
                        .add(new AttributeAppender("class", " " + stateCss)));
            }
        });
//        add(new PageablePanel("pagingPanel", this, projectsModel));
        add(new WebMarkupContainer("noProjects"));
    }

    @Override
    protected void onConfigure() {

        var foundProjects = projectsModel.getTotalElements() > 0;
        get("noProjects").setVisible(!foundProjects);
        get("projects").setVisible(foundProjects);

        super.onConfigure();
    }

}
