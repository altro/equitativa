package com.equitativa.testwork.wicket.component.header;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

import com.equitativa.testwork.wicket.TestWorkWebSession;
import com.equitativa.testwork.wicket.page.MainPage;
import com.equitativa.testwork.wicket.page.auth.SignInPage;
import com.equitativa.testwork.wicket.page.auth.SignOutPage;

public class HeaderPanel extends Panel {

    public HeaderPanel(String id) {
        super(id);

        add(new BookmarkablePageLink<>("dashboardLink", MainPage.class));

        add(new BookmarkablePageLink<>("signInLink", SignInPage.class));
        add(new BookmarkablePageLink<>("signOutLink", SignOutPage.class));
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        var isLoggedIn = TestWorkWebSession.getSession().isLoggedIn();

        get("dashboardLink").setVisible(isLoggedIn);
        get("signOutLink").setVisible(isLoggedIn);
        get("signInLink").setVisible(!isLoggedIn);
    }

}
