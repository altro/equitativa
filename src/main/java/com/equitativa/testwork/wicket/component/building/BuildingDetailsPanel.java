package com.equitativa.testwork.wicket.component.building;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import lombok.Data;

import com.equitativa.testwork.model.Building;
import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.service.UserService;
import com.equitativa.testwork.wicket.model.HibernateModel;
import com.equitativa.testwork.wicket.page.MainPage;

import static com.equitativa.testwork.util.WicketUtil.toNamedParams;

public class BuildingDetailsPanel extends Panel {

    private static final UserEntry USER_NOT_SELECTED = new UserEntry(0L, "");

    private UserEntry selectedAssignedUser;
    private IModel<Building> buildingModel;

    public BuildingDetailsPanel(String id, Long buildingId, Long userId) {
        super(id);
        buildingModel = new HibernateModel<>(Building.class, buildingId);

        if (userId != null) {
            selectedAssignedUser = new UserEntry(userId, "");
        }

        add(new Label("subtitle", () -> getString("label.subTitle", new CompoundPropertyModel<>(Map.of("buildingTitle", buildingModel.getObject().getTitle())))));

        var usersModel = new LoadableDetachableModel<List<UserEntry>>() {
            @Override
            protected List<UserEntry> load() {
                var users = UserService.get().getUsersByRole(UserRole.WORKER);
                var result = new ArrayList<UserEntry>();
                for (var user : users) {
                    result.add(new UserEntry(user.getId(), user.getName()));
                }

                return result;
            }
        };

        ProjectsListPanel projectsPanel;
        add(projectsPanel = new ProjectsListPanel("projects", Model.of(selectedAssignedUser != null ? selectedAssignedUser.id : null), buildingId));
        projectsPanel.setOutputMarkupId(true);

        DropDownChoice<UserEntry> assignedUserFilter;
        add(assignedUserFilter = new DropDownChoice<>("assignedUserFilter",
                new PropertyModel<>(this, "selectedAssignedUser"),
                usersModel,
                new IChoiceRenderer<>() {
                    @Override
                    public Object getDisplayValue(UserEntry userEntry) {
                        return userEntry.name;
                    }

                    @Override
                    public String getIdValue(UserEntry userEntry, int index) {
                        return userEntry.id.toString();
                    }
                }));
        assignedUserFilter.setNullValid(true);

        assignedUserFilter.add(new AjaxFormComponentUpdatingBehavior("change") {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                // TODO: Need to understand, why this logic doesn't work
/*
                projectsPanel.setDefaultModelObject(selectedAssignedUser != null ? selectedAssignedUser.id : null);
                target.add(projectsPanel);
*/
                setResponsePage(MainPage.class, toNamedParams("id", buildingId, "userId", selectedAssignedUser != null ? selectedAssignedUser.id : null));
            }
        });

    }

    @Override
    protected void onConfigure() {

        setVisible(buildingModel.getObject() != null);

        super.onConfigure();
    }

    @Data
    private static class UserEntry implements Serializable {

        private final Long id;
        private final String name;
    }
}
