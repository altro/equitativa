package com.equitativa.testwork.wicket.component.building;

import java.util.Collections;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

import com.equitativa.testwork.model.Building;
import com.equitativa.testwork.model.ProjectState;
import com.equitativa.testwork.service.BuildingService;
import com.equitativa.testwork.wicket.model.PageableModel;
import com.equitativa.testwork.wicket.page.MainPage;

import static com.equitativa.testwork.util.WicketUtil.toIdParams;

public class BuildingsListPanel extends Panel {

    private PageableModel<Building> buildingsModel;

    public BuildingsListPanel(String id) {
        super(id);

        add(new ListView<>("buildings", buildingsModel = new PageableModel<>(15) {
            @Override
            protected int getElementsCount() {
                return BuildingService.get().count();
            }

            @Override
            protected List<Building> doLoad(int first, int count) {
                if (getTotalElements() == 0) {
                    return Collections.emptyList();
                }
                return BuildingService.get().find(first, count);
            }
        }) {
            @Override
            protected void populateItem(ListItem<Building> listItem) {
                var building = listItem.getModelObject();

                // TODO: move this logic to service. Need to add in memory entitiee,
                //  with calculated progress
                //  like BuildingInfo and ProjectInfo
                var projects = building.getProjects();
                var total = projects.size();
                var completed = 0;
                var inProgress = 0;
                for (var project : projects) {
                    if (project.getState() == ProjectState.COMPLETED) {
                        completed++;
                    } else if (project.getState() == ProjectState.NEW) {
                        //
                    } else {
                        inProgress++;
                    }
                }

                var progress = (int) ((completed / (double) total) * 100);

                listItem.add(new Label("buildingId", building.getId()))
                        .add(new Label("title", building.getTitle()))
                        .add(new Label("progress", progress + "%")
                                .add(new AttributeModifier("style", "width:" + progress + "%"))
                                .add(new AttributeModifier("aria-valuenow", progress))
                        )
                        .add(new BookmarkablePageLink<>("view", MainPage.class, toIdParams(building.getId())))
                ;
            }
        });
//        add(new PageablePanel("pagingPanel", this, buildingsModel));
        add(new WebMarkupContainer("noBuildings"));
    }

    @Override
    protected void onConfigure() {

        var foundBuildings = buildingsModel.getTotalElements() > 0;
        get("noBuildings").setVisible(!foundBuildings);
        get("buildings").setVisible(foundBuildings);
        super.onConfigure();
    }
}
