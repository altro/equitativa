package com.equitativa.testwork.wicket.page.auth;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.service.UserService;
import com.equitativa.testwork.util.StringUtil;
import com.equitativa.testwork.util.access.Restricted;
import com.equitativa.testwork.wicket.RestartResponseNoRedirectException;
import com.equitativa.testwork.wicket.TestWorkWebSession;
import com.equitativa.testwork.wicket.page.BasePage;
import com.equitativa.testwork.wicket.page.MainPage;

@Slf4j
@Restricted(access = UserRole.GUEST)
public class SignInPage extends BasePage {

    public SignInPage(PageParameters pageParameters) {
        super(pageParameters);

        var session = TestWorkWebSession.getSession();
        if (session.isLoggedIn()) {
            log.debug("Redirecting user {} to MainPage", session.getUserId());
            throw new RestartResponseNoRedirectException(MainPage.class);
        }

        Form<?> form;
        add(form = new StatelessForm<>("signInForm") {

            private TextField<String> loginField;
            private TextField<String> passwordField;

            {
                add(loginField = new RequiredTextField<>("login", Model.of("")));
                add(passwordField = new PasswordTextField("password", Model.of("")));
            }

            @Override
            protected void onSubmit() {
                var password = StringUtil.trimToEmpty(passwordField.getModelObject());
                var login = loginField.getModelObject();
                if (StringUtil.isEmpty(login) || StringUtil.isEmpty(password)) {
                    error(getString("emptyLoginOrPassword"));
                    return;
                }

                WebRequest webRequest = getWebRequest();
                HttpServletRequest request = ((ServletWebRequest) webRequest).getContainerRequest();

                var webSession = TestWorkWebSession.getSession();
                var user = UserService.get().loginUserByPassword(login, password);
                if (user == null) {
                    error(getString("invalidLoginOrPassword"));
                    return;
                }

                webSession.setUser(user.getId(), user.getRole());
                continueToOriginalDestination();
                setResponsePage(MainPage.class);
            }
        });

        add(new BookmarkablePageLink<>("signUpLink", SignUpPage.class));
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        get("bp.header").setVisible(false);
    }

}
