package com.equitativa.testwork.wicket.page;

import org.apache.wicket.Component;
import org.apache.wicket.feedback.FencedFeedbackPanel;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.wicket.TestWorkWebSession;
import com.equitativa.testwork.wicket.component.header.HeaderPanel;

@Slf4j
public class BasePage extends WebPage {

    private boolean hasMessages;
    protected Component topPanel;
    protected WebMarkupContainer footer;
    protected WebMarkupContainer footMenu;

    public BasePage() {
    }

    public BasePage(IModel<?> model) {
        super(model);
    }

    public BasePage(PageParameters pageParameters) {
        super(pageParameters);
    }

    @Override
    public void onInitialize() {
        super.onInitialize();

        add(new HeaderPanel("bp.header"));
    }

    @Override
    protected void onConfigure() {

        var webRequest = getWebRequest();
        var session = getTestWorkSession();
        var isLoggedIn = session.isLoggedIn();

        super.onConfigure();
    }

    @Override
    protected void onBeforeRender() {

        var session = getTestWorkSession();
        var isLoggedIn = session.isLoggedIn();

        refreshFeedBackPanel();

        super.onBeforeRender();
    }

    public WebRequest getWebRequest() {
        return (WebRequest) getRequest();
    }

    public TestWorkWebSession getTestWorkSession() {
        return (TestWorkWebSession) super.getSession();
    }

    private void refreshFeedBackPanel() {
        var feedBackPanelId = "bp.feedback";
        var currentFeedbackPanel = get(feedBackPanelId);
        if (currentFeedbackPanel != null) {
            currentFeedbackPanel.remove();
        }
        add(createFeedbackPanel(feedBackPanelId));
    }

    private Component createFeedbackPanel(String wid) {

        hasMessages = !getTestWorkSession().getFeedbackMessages().isEmpty();
        visitChildren((object, visit) -> {
            if (object.hasFeedbackMessage()) {
                hasMessages = true;
            }
        });
        if (hasMessages) {
            return new FencedFeedbackPanel(wid, IFeedbackMessageFilter.ALL)
                    .setEscapeModelStrings(false);
        }

        return new WebMarkupContainer(wid).setVisible(false);
    }
}
