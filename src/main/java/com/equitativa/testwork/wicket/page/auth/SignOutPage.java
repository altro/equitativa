package com.equitativa.testwork.wicket.page.auth;

import org.apache.wicket.Page;

import com.equitativa.testwork.wicket.TestWorkApplication;
import com.equitativa.testwork.wicket.TestWorkWebSession;

public class SignOutPage extends Page {

    public SignOutPage() {
        var session = TestWorkWebSession.getSession();
        var userInfo = session.isLoggedIn();

        if (session.isLoggedIn()) {
            session.logoutUser();
        }
        setResponsePage(TestWorkApplication.get().getHomePage());
    }

}
