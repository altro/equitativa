package com.equitativa.testwork.wicket.page;

import org.apache.wicket.request.mapper.parameter.PageParameters;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.util.access.Restricted;
import com.equitativa.testwork.wicket.RestartResponseNoRedirectException;
import com.equitativa.testwork.wicket.TestWorkWebSession;

@Slf4j
@Restricted(access = UserRole.GUEST)
public class HomePage extends BasePage {

    public HomePage(PageParameters pageParameters) {
        super(pageParameters);

        var session = TestWorkWebSession.getSession();
        if (session.isLoggedIn()) {
            log.debug("Redirecting user {} to MainPage", session.getUserId());
            throw new RestartResponseNoRedirectException(MainPage.class);
        }

    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
    }
}
