package com.equitativa.testwork.wicket.page;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.wicket.component.building.BuildingDetailsPanel;
import com.equitativa.testwork.wicket.component.building.BuildingsListPanel;

@Slf4j
public class MainPage extends BasePage {

    private Long userId;
    private Long buildingId;

    public MainPage(PageParameters pageParameters) {
        super(pageParameters);

        buildingId = pageParameters.get("id").toLong(0);
        if (buildingId <= 0) {
            buildingId = null;
        }
        userId = pageParameters.get("userId").toLong(0);
        if (userId <= 0) {
            userId = null;
        }

        add(new BuildingsListPanel("buildings"));
        if (buildingId != null) {
            add(new BuildingDetailsPanel("buildingDetails", buildingId, userId));
        } else {
            add(new WebMarkupContainer("buildingDetails"));
        }
    }

    @Override
    protected void onConfigure() {

        get("buildingDetails").setVisible(buildingId != null);

        super.onConfigure();
    }
}
