package com.equitativa.testwork.wicket.page.auth;

import java.util.List;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.service.UserService;
import com.equitativa.testwork.util.StringUtil;
import com.equitativa.testwork.util.access.Restricted;
import com.equitativa.testwork.wicket.RestartResponseNoRedirectException;
import com.equitativa.testwork.wicket.TestWorkWebSession;
import com.equitativa.testwork.wicket.page.BasePage;
import com.equitativa.testwork.wicket.page.MainPage;

@Slf4j
@Restricted(access = UserRole.GUEST)
public class SignUpPage extends BasePage {

    public SignUpPage(PageParameters pageParameters) {
        super(pageParameters);

        var session = TestWorkWebSession.getSession();
        if (session.isLoggedIn()) {
            log.debug("Redirecting user {} to MainPage", session.getUserId());
            throw new RestartResponseNoRedirectException(MainPage.class);
        }

        Form<?> form;
        add(form = new Form<>("form") {

            private TextField<String> loginField;
            private PasswordTextField passwordField;
            private DropDownChoice<UserRole> roleField;

            {
                add(loginField = new RequiredTextField<>("login", Model.of("")));
                loginField.add(StringValidator.maximumLength(100));

                add(passwordField = new PasswordTextField("password", Model.of("")));
                passwordField.add(StringValidator.minimumLength(6));

                add(roleField = new DropDownChoice<>("role", Model.of(UserRole.USER),
                        UserRole.ROLES,
                        new IChoiceRenderer<>() {
                            @Override
                            public Object getDisplayValue(UserRole role) {
                                return getString("role." + role);
                            }

                            @Override
                            public String getIdValue(UserRole role, int index) {
                                return String.valueOf(role.getCode());
                            }

                            @Override
                            public UserRole getObject(String id, IModel<? extends List<? extends UserRole>> choices) {
                                return UserRole.byCode(id.charAt(0));
                            }
                        }));
                roleField.setNullValid(false);
            }

            @Override
            protected void onSubmit() {
                var login = loginField.getModelObject();
                if (StringUtil.isBlank(login)) {
                    error(getString("error.emptyLogin"));
                    return;
                }

                var password = StringUtil.trimToEmpty(passwordField.getModelObject());
                if (StringUtil.isBlank(password)) {
                    error(getString("error.emptyPassword"));
                    return;
                }

                var role = roleField.getModelObject();
                if (role == null) {
                    error(getString("error.roleNotSelected"));
                    return;
                }

                var user = UserService.get().createUser(login, password, role);
                if (user == null) {
                    error(getString("error.invalidData"));
                    return;
                }

                TestWorkWebSession.getSession().setUser(user.getId(), user.getRole());
                setResponsePage(MainPage.class);
            }
        });

        form.add(new BookmarkablePageLink<>("cancel", getApplication().getHomePage()));
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();

        get("bp.header").setVisible(false);
    }
}
