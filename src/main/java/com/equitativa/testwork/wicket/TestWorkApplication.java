package com.equitativa.testwork.wicket;

import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Properties;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.coep.CrossOriginEmbedderPolicyConfiguration;
import org.apache.wicket.coop.CrossOriginOpenerPolicyConfiguration;
import org.apache.wicket.core.request.handler.ListenerInvocationNotAllowedException;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.request.handler.RenderPageRequestHandler;
import org.apache.wicket.core.request.mapper.StalePageException;
import org.apache.wicket.markup.html.pages.AccessDeniedPage;
import org.apache.wicket.markup.html.pages.InternalErrorPage;
import org.apache.wicket.protocol.http.RequestLoggerRequestCycleListener;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.IRequestHandler;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.cycle.IRequestCycleListener;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.settings.ExceptionSettings;
import org.apache.wicket.settings.RequestCycleSettings;
import org.apache.wicket.util.lang.Bytes;
import org.slf4j.LoggerFactory;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.util.TimeUtil;
import com.equitativa.testwork.wicket.page.HomePage;
import com.equitativa.testwork.wicket.page.MainPage;
import com.equitativa.testwork.wicket.page.auth.SignInPage;
import com.equitativa.testwork.wicket.page.auth.SignOutPage;
import com.equitativa.testwork.wicket.page.auth.SignUpPage;

@NoArgsConstructor
@Slf4j
public class TestWorkApplication extends WebApplication {

    private static boolean destroy;
    private static boolean devMode;

    private static final long creationTime = System.currentTimeMillis();

    @Override
    public Session newSession(Request request, Response response) {
        return new TestWorkWebSession(request);
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();

        HibernateContext.get();

        devMode = usesDevelopmentConfig();
        System.setProperty("production", Boolean.toString(usesDeploymentConfig()));

        log.info("*********************************************");
        try {
            initLogger();
            configureSystemPrps();
            configureWebApplication();
            initMounts();
            log.info("Initialized success >>>>>>>>>");
        } catch (Throwable t) {
            log.info("Initialization failed with errors:", t);
            log.info(">>>>>>>>>");
            throw new RuntimeException("Initialization failed with errors:", t);
        }
    }

    private void initMounts() {

        mountPage("/signin", SignInPage.class);
        mountPage("/signup", SignUpPage.class);
        mountPage("/signout", SignOutPage.class);
        mountPage("/app/#{id}/#{userId}", MainPage.class);

        mountPage("/err/internal", InternalErrorPage.class);
        mountPage("/err/denied", AccessDeniedPage.class);
    }

    private void initLogger() {
        // init logger
        var context = (LoggerContext) LoggerFactory.getILoggerFactory();
        log.info("Initializing logging");

        try {
            var configurator = new JoranConfigurator();
            configurator.setContext(context);
            context.reset();
            configurator.doConfigure(getClass().getClassLoader().getResourceAsStream("logback.xml"));
        } catch (JoranException je) {
            throw new RuntimeException(je);
        }
    }

    public void configureSystemPrps() {
        log.info("Configuration loading default system properties...");

        try (var systemPropsStream = getClass().getClassLoader().getResourceAsStream("application.properties")) {
            var systemProps = new Properties();
            systemProps.load(systemPropsStream);
            System.setProperties(systemProps);
            log.info("Configuration system properties load success.");
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private void configureWebApplication() {

        //settings
        var markupSettings = getMarkupSettings()
                .setDefaultMarkupEncoding(StandardCharsets.UTF_8.name());

        getSecuritySettings()
                .setCrossOriginEmbedderPolicyConfiguration(CrossOriginEmbedderPolicyConfiguration.CoepMode.DISABLED)
                .setCrossOriginOpenerPolicyConfiguration(CrossOriginOpenerPolicyConfiguration.CoopMode.SAME_ORIGIN)
                .setAuthorizationStrategy(new TestWorkAuthorizationStrategy())
                .setEnforceMounts(false);

        getCspSettings()
                .blocking().disabled();

        getResourceSettings()
                .setDefaultCacheDuration(Duration.ofMinutes(Integer.parseInt(System.getProperty("wicket.defaultCacheDuration"))))
                .setResourcePollFrequency(Duration.ofMinutes(Integer.parseInt(System.getProperty("wicket.resourcePollFrequency"))))
                .setUseDefaultOnMissingResource(true)
                .setThrowExceptionOnMissingResource(devMode)
                .setUseMinifiedResources(false);

        if (devMode) {

//            getComponentInstantiationListeners().add(new RenderPerformanceListener());
//            getComponentPostOnBeforeRenderListeners().add(new StatelessChecker());
            getDebugSettings()
                    .setDevelopmentUtilitiesEnabled(true);

            getRequestLoggerSettings()
                    .setRequestLoggerEnabled(true)
                    .setRequestsWindowSize(3000);

            getRequestCycleListeners()
                    .add(new RequestLoggerRequestCycleListener());

            markupSettings
                    .setThrowExceptionOnMissingXmlDeclaration(true);
        } else {
            markupSettings
                    .setCompressWhitespace(true)
                    .setStripComments(true)
                    .setStripWicketTags(true)
                    .setThrowExceptionOnMissingXmlDeclaration(false);
        }

        getApplicationSettings()
                .setInternalErrorPage(InternalErrorPage.class)
//                .setPageExpiredErrorPage(ExpiredPage.class)
                .setAccessDeniedPage(AccessDeniedPage.class);

        getStoreSettings()
                .setMaxSizePerSession(Bytes.bytes(Long.parseLong(System.getProperty("wicket.maxSizePerSession"))));

        getPageSettings()
                .setVersionPagesByDefault(false)
                .setRecreateBookmarkablePagesAfterExpiry(true);

        getExceptionSettings()
                .setThreadDumpStrategy(ExceptionSettings.ThreadDumpStrategy.ALL_THREADS);


        getRequestCycleListeners()
                .add(new IRequestCycleListener() {

                    @Override
                    public IRequestHandler onException(RequestCycle cycle, Exception e) {
                        if (e instanceof StalePageException) {
                            return null;
                        }
                        if (e instanceof ListenerInvocationNotAllowedException) {
                            Component component = ((ListenerInvocationNotAllowedException) e).getComponent();
                            if (component != null) {
                                log.warn("ListenerInvocationNotAllowedException for component {}. Redirect to page {}, params {}", component, component.getPage(), component.getPage().getPageParameters());
                                return new RenderPageRequestHandler(new PageProvider(component.getPage().getPageClass(), component.getPage().getPageParameters()), RenderPageRequestHandler.RedirectPolicy.ALWAYS_REDIRECT);
                            }
                        }
                        log.error("Error: ", e);
                        var t = e.getCause();
                        while (t != null) {
                            log.error("Nested error: ", t);
                            t = t.getCause();
                        }
                        return null;
                    }
                });

        getRequestCycleSettings()
                .setResponseRequestEncoding(StandardCharsets.UTF_8.toString())
                .setRenderStrategy(RequestCycleSettings.RenderStrategy.REDIRECT_TO_RENDER)
                .setTimeout(Duration.ofSeconds(Integer.parseInt(System.getProperty("wicket.requestTimeout"))));

        setRequestCycleProvider(TestWorkRequestCycle::new);
    }

    @Override
    protected void onDestroy() {
        try {

            log.info("Destroy started...");

            destroy = true;

            var hibernateContext = HibernateContext.get();
            hibernateContext.closeSession(true);
            hibernateContext.destroy();

            var drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                var driver = drivers.nextElement();
                try {
                    DriverManager.deregisterDriver(driver);
                    log.info("Deregistering JDBC driver: {}", driver);
                } catch (SQLException e) {
                    log.error("Error deregistering driver {}", driver, e);
                }
            }

            log.info("Destroy finished success. >>>>>>>>>");
        } catch (Throwable t) {
            log.info("Destroy failed with errors:", t);
            log.info("Destroy failed. >>>>>>>>>");
        }
    }

    public static TestWorkApplication get() {
        return (TestWorkApplication) WebApplication.get();
    }

    public static boolean justStarted() {
        return getWorkTime() < TimeUtil.MINUTE_MILLIS;
    }

    public static long getWorkTime() {
        return System.currentTimeMillis() - creationTime;
    }

    public static boolean isDevMode() {
        return devMode;
    }

    public static boolean isDestroy() {
        return destroy;
    }

}
