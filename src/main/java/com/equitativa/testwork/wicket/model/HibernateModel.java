package com.equitativa.testwork.wicket.model;

import java.io.Serializable;

import org.apache.wicket.model.IModel;
import org.hibernate.proxy.HibernateProxyHelper;

import com.equitativa.testwork.dao.hibernate.HibernateContext;

public class HibernateModel<T> implements IModel<T> {

    private Class<T> clazz;
    private Serializable id;

    public HibernateModel(Class<T> clazz, Serializable id) {
        this.clazz = clazz;
        this.id = id;
    }

    @Override
    public T getObject() {
        if (id instanceof Number) {
            if (((Number) id).intValue() <= 0) {
                return null;
            }
        }
        return HibernateContext.get().getSession().get(clazz, id);
    }

    public static <T> HibernateModel<T> of(Class<T> clazz, Serializable id) {
        return new HibernateModel<>(clazz, id);
    }

    @SuppressWarnings({"unchecked"})
    public static <T> HibernateModel<T> of(T obj) {
        Serializable id = HibernateContext.get().getSession().getIdentifier(obj);
        return of((Class<T>) HibernateProxyHelper.getClassWithoutInitializingProxy(obj), id);
    }
}
