package com.equitativa.testwork.wicket.model;

import java.util.List;

import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

public abstract class PageableModel<T> extends LoadableDetachableModel<List<T>> implements IPageable {

    private int currentPage;
    private int pageSize;

    private IModel<Integer> countModel = new LoadableDetachableModel<>() {
        @Override
        protected Integer load() {
            return getElementsCount();
        }
    };

    protected PageableModel(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public long getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setCurrentPage(long currentPage) {
        this.currentPage = (int) currentPage;
    }

    @Override
    public long getPageCount() {
        long count = getTotalElements();
        return (count % pageSize) == 0 ? (count / pageSize) : (count / pageSize + 1);
    }

    public long getPageSize() {
        return pageSize;
    }

    @Override
    protected final List<T> load() {
        return doLoad(pageSize * currentPage, pageSize);
    }

    public long getTotalElements() {
        return countModel.getObject();
    }

    public IModel<Integer> getTotalElementsModel() {
        return countModel;
    }

    protected abstract int getElementsCount();

    protected abstract List<T> doLoad(int firstElement, int elementsCount);

    @Override
    protected void onDetach() {
        countModel.detach();
    }
}
