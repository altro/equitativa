package com.equitativa.testwork.wicket;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.authorization.Action;
import org.apache.wicket.authorization.IAuthorizationStrategy;
import org.apache.wicket.markup.html.pages.AccessDeniedPage;
import org.apache.wicket.protocol.http.request.WebClientInfo;
import org.apache.wicket.request.component.IRequestableComponent;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.IResource;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.util.access.AccessUtil;
import com.equitativa.testwork.wicket.page.BasePage;
import com.equitativa.testwork.wicket.page.HomePage;

@Slf4j
public class TestWorkAuthorizationStrategy implements IAuthorizationStrategy {

    @Override
    public <T extends IRequestableComponent> boolean isInstantiationAuthorized(Class<T> componentClass) {
        if (Page.class.isAssignableFrom(componentClass)) {
            checkAccess(componentClass);
        }
        return true;
    }

    @Override
    public boolean isActionAuthorized(Component component, Action action) {
        if (!(component instanceof Page)) {
            return true;
        }
        Page page = (Page) component;
        checkAccess(page.getClass());

        return true;
    }

    @Override
    public boolean isResourceAuthorized(IResource resource, PageParameters parameters) {
        return true;
    }

    private void checkAccess(Class pageClass) {

        var session = TestWorkWebSession.getSession();

        var userId = session.getUserId();
        boolean isBasePage = BasePage.class.isAssignableFrom(pageClass);

        boolean isGuestPage = AccessUtil.isAccessContains(pageClass, UserRole.GUEST);
        boolean isAdminPage = AccessUtil.isAccessContains(pageClass, UserRole.ADMIN);

        boolean isLoggedIn = userId > 0;
        boolean isAdmin = false;
        if (isLoggedIn) {
            isAdmin = session.getRole() == UserRole.ADMIN;
        }

        if (isAdminPage && !isAdmin) {
            log.info("Rejected admin page {}", pageClass.getName());
            throw new RestartResponseAtInterceptPageException(AccessDeniedPage.class);
        }
        if (isBasePage && !isLoggedIn && !isGuestPage) {
            WebClientInfo clientInfo = session.getClientInfo();
            log.info("Rejected {}, IP address {}, User-Agent {}", pageClass.getName(), clientInfo == null ? "unknown" : clientInfo.getProperties().getRemoteAddress(), clientInfo == null ? "unknown" : clientInfo.getUserAgent());
            throw new RestartResponseAtInterceptPageException(HomePage.class);
        }
    }

}
