package com.equitativa.testwork.wicket;

import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.cycle.RequestCycle;

import lombok.Getter;

import com.equitativa.testwork.model.UserRole;

@Getter
public class TestWorkWebSession extends WebSession {

    private long userId;
    private UserRole role;

    private long masterUserId;
    private UserRole masterUserRole;

    public TestWorkWebSession(Request request) {
        super(request);
    }

    public static TestWorkWebSession getSession() {
        return (TestWorkWebSession) WebSession.get();
    }

    public void setUser(long newUserId, UserRole role) {
        if (newUserId <= 0) {
            return;
        }

        long oldId = userId;

        userId = newUserId;
        this.role = role;

        if (userId != oldId) {
            clear();
        }

//        userInfo.updateLastActionTime();
//        userInfo.updateLastLoginTime();

        createRealSession();
    }

    public void loginAs(long loginUserId, UserRole loginUserRole) {
        if (loginUserId <= 0) {
            return;
        }
        if (masterUserId != 0) {
            // already logged as another user
            return;
        }

        masterUserId = userId;

        setUser(loginUserId, loginUserRole);
    }

    public void restoreMasterLogin() {
        if (masterUserId == 0) {
            // not logged as another user
            return;
        }

        userId = masterUserId;
        role = masterUserRole;
        masterUserId = 0;
        masterUserRole = null;
    }

    private void createRealSession() {
//        bind();
        ((ServletWebRequest) RequestCycle.get().getRequest()).getContainerRequest().getSession(true);
    }

    public boolean isLoggedIn() {
        return userId > 0;
    }

    public boolean logoutUser() {
        userId = 0;
        role = null;
        masterUserId = 0;
        masterUserRole = null;
        return true;
    }

    public boolean isAdmin() {
        return role == UserRole.ADMIN;
    }

    public boolean isWorker() {
        return role == UserRole.WORKER;
    }

    public boolean isValuable() {
        return userId > 0;
    }
}
