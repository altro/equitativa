package com.equitativa.testwork.service;

import java.util.Collections;
import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.ProjectDao;
import com.equitativa.testwork.model.Project;
import com.equitativa.testwork.service.data.ProjectFilter;

@Slf4j
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectDao projectDao;

    private static ProjectService instance;

    public static ProjectService get() {
        if (instance == null) {
            instance = new ProjectService(ProjectDao.get());
        }

        return instance;
    }

    public int countProjects(Long buildingId, Long assignedUserId) {
        if (buildingId == null || buildingId <= 0) {
            return 0;
        }
        if (assignedUserId == null || assignedUserId <= 0) {
            return projectDao.getCountByBuildingId(buildingId);
        }
        return projectDao.getCountByBuildingIdAndAssignedUserId(buildingId, assignedUserId);
    }

    public List<Project> findProjects(Long buildingId, Long assignedUserId, int first, int count) {
        if (buildingId == null || buildingId <= 0) {
            return Collections.emptyList();
        }
        if (assignedUserId == null || assignedUserId <= 0) {
            return projectDao.findByBuildingId(buildingId, first, count);
        }
        return projectDao.findByBuildingIdAndAssignedUserId(buildingId, assignedUserId, first, count);
    }

    public List<Project> getProjects(ProjectFilter filter) {

        return Collections.emptyList();
    }
}
