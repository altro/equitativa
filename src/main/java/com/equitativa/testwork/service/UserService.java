package com.equitativa.testwork.service;

import java.time.LocalDateTime;
import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.UserDao;
import com.equitativa.testwork.model.Status;
import com.equitativa.testwork.model.User;
import com.equitativa.testwork.model.UserRole;
import com.equitativa.testwork.util.StringUtil;

import static com.equitativa.testwork.util.StringUtil.getSalt;
import static com.equitativa.testwork.util.StringUtil.hexToArray;
import static com.equitativa.testwork.util.StringUtil.toHex;

@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserDao userDao;

    private static UserService instance;

    public static UserService get() {
        if (instance == null) {
            instance = new UserService(UserDao.get());
        }

        return instance;
    }

    public User createUser(String login, String password, UserRole role) {

        if (StringUtil.isBlank(login)
                || StringUtil.isBlank(password)
                || role == null
                || role == UserRole.GUEST
        ) {
            log.error("Can't create user login={}, password={}, role={}", login, password, role);
            return null;
        }

        log.debug("Registering user {}", login);

        var salt = getSalt();
        var encodedPassword = StringUtil.sha256(password, salt);
        var now = LocalDateTime.now();

        var user = User.builder()
                .status(Status.ACTIVE)
                .login(login)
                .password(encodedPassword)
                .salt(toHex(salt))
                .createTime(now)
                .updateTime(now)
                .role(role)
                .build();

        var userId = userDao.save(user);
        user.setId(userId);

        log.info("Registered user {}:{}", login, userId);

        return user;
    }

    public List<User> getUsersByRole(UserRole role) {
        return userDao.getByRole(role);
    }

    public User loginUserByPassword(String login, String password) {

        if (StringUtil.isEmpty(login) || StringUtil.isEmpty(password)) {
            return null;
        }
        var user = getUserByLogin(login);
        if (user == null) {
            return null;
        }

        if (!verifyPassword(user, password)) {
            return null;
        }

        log.info("User {} logged in", login);

        user.setLastLoginTime(LocalDateTime.now());
        userDao.save(user);

        return user;
    }

    public User getUserByLogin(String login) {

        if (StringUtil.isEmpty(login)) {
            return null;
        }

        return userDao.getByLogin(login.toLowerCase().trim());
    }

    private boolean verifyPassword(User user, String password) {
        if (StringUtil.isEmpty(user.getPassword())) {
            // password isn't defined
            return false;
        }

        var salt = user.getSalt();
        var encodedPassword = StringUtil.sha256(password, hexToArray(salt));

        return encodedPassword.equals(user.getPassword().trim());
    }
}
