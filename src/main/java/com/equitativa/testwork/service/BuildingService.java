package com.equitativa.testwork.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.BuildingDao;
import com.equitativa.testwork.model.Building;

@Slf4j
@RequiredArgsConstructor
public class BuildingService {

    private final BuildingDao buildingDao;

    private static BuildingService instance;

    public static BuildingService get() {
        if (instance == null) {
            instance = new BuildingService(BuildingDao.get());
        }

        return instance;
    }

    public int count() {
        return buildingDao.getCount();
    }

    public List<Building> find(int first, int count) {
        return buildingDao.find(first, count);
    }
}
