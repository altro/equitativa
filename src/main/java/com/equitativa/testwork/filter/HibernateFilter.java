package com.equitativa.testwork.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.hibernate.HibernateContext;

@Slf4j
public class HibernateFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException {

        var request = (HttpServletRequest) servletRequest;
        var hibernateContext = HibernateContext.get();
        try {
            filterChain.doFilter(servletRequest, servletResponse);

            hibernateContext.enableFlush();
            hibernateContext.closeSession(true);
        } catch (Throwable t) {
            hibernateContext.enableFlush();
            hibernateContext.closeSession(false);
            if (log.isErrorEnabled()) {
                log.error("Exception occurred, transaction rolled back. IP: {} User-Agent: {} URI: {} QueryString: {}",
                        request.getRemoteAddr(), request.getHeader("User-Agent"), request.getRequestURI(), request.getQueryString());
            }
            throw new ServletException("", t);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
