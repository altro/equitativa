package com.equitativa.testwork.util.access;

import com.equitativa.testwork.model.UserRole;

public class AccessUtil {

    public static boolean isAccessContains(Class<?> pageClass, UserRole access) {
        Class<?> cl = pageClass;
        while (cl != null) {
            var restricted = cl.getAnnotation(Restricted.class);
            if (restricted != null && restricted.access() == access) {
                return true;
            }
            cl = cl.getSuperclass();
        }
        return false;
    }

}
