package com.equitativa.testwork.util.access;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.equitativa.testwork.model.UserRole;

@Retention(RetentionPolicy.RUNTIME)
public @interface Restricted {

    UserRole access();

}
