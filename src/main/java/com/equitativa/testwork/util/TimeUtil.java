package com.equitativa.testwork.util;

import java.util.concurrent.TimeUnit;

public class TimeUtil {

    public static final long SECOND_MILLIS = TimeUnit.SECONDS.toMillis(1);
    public static final long MINUTE_MILLIS = TimeUnit.MINUTES.toMillis(1);

}
