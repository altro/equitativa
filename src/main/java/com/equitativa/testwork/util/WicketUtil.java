package com.equitativa.testwork.util;

import org.apache.wicket.request.mapper.parameter.PageParameters;

public class WicketUtil {

    public static PageParameters toIdParams(Object id) {
        var pageParameters = new PageParameters();
        if (id != null) {
            pageParameters.add("id", id.toString());
        }
        return pageParameters;
    }

    public static PageParameters toNamedParams(Object... values) {
        var pageParameters = new PageParameters();

        if (values != null && values.length > 0) {
            int size = values.length / 2;
            for (int i = 0; i < size; i++) {
                Object paramName = values[2 * i];

                if (paramName != null) {
                    var paramValue = values[2 * i + 1];
                    pageParameters.add(paramName.toString(), paramValue == null ? "" : paramValue.toString());
                }
            }
        }

        return pageParameters;
    }

}
