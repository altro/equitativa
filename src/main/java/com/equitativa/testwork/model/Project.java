package com.equitativa.testwork.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.equitativa.testwork.model.hibernate.StatusConverter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "projects")
@Entity
public class Project {

    @Id
    @SequenceGenerator(name = "projects_id_seq", sequenceName = "projects_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projects_id_seq")
    private Long id;
    @Convert(converter = StatusConverter.class)
    @Column(nullable = false)
    @NotNull
    private Status status;
    @Enumerated(EnumType.STRING)
    private ProjectState state;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ProjectType type;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @NotNull
    private Building building;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private User assignedUser;

    private LocalDateTime startTime;
    private LocalDateTime finishTime;

    @Column(nullable = false)
    @NotNull
    private LocalDateTime createTime;
    @Column(nullable = false)
    @NotNull
    private LocalDateTime updateTime;

    public Long getBuildingId() {
        return building != null ? building.getId() : null;
    }

    public void setBuildingId(Long buildingId) {
        building = new Building(buildingId);
    }

    public Long getAssignedUserId() {
        return assignedUser != null ? assignedUser.getId() : null;
    }

    public void setAssignedUserId(Long assignedUserId) {
        assignedUser = new User(assignedUserId);
    }

    public Long getTypeId() {
        return type != null ? type.getId() : null;
    }

    public void setTypeId(Long typeId) {
        type = new ProjectType(typeId);
    }
}
