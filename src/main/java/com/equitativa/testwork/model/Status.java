package com.equitativa.testwork.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Getter
public enum Status {

    ACTIVE('A'),
    REMOVED('R');

    public static final Map<Character, Status> BY_CODE;

    static {

        var byCode = new HashMap<Character, Status>();
        for (var value : values()) {
            byCode.put(value.code, value);
        }

        BY_CODE = Map.copyOf(byCode);
    }

    private final Character code;

    public static Status byCode(char code) {
        var value = BY_CODE.get(code);
        if (value == null) {
            log.error("Unknown code: {}", code);
            return ACTIVE;
        }
        return value;
    }

}
