package com.equitativa.testwork.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.equitativa.testwork.model.hibernate.StatusConverter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "buildings")
@Entity
public class Building {

    @Id
    @SequenceGenerator(name = "buildings_id_seq", sequenceName = "buildings_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "buildings_id_seq")
    private Long id;
    @Convert(converter = StatusConverter.class)
    @Column(nullable = false)
    @NotNull
    private Status status;
    @Column(nullable = false)
    @NotBlank
    private String code;
    @Column(nullable = false)
    @NotBlank
    private String title;

    @OneToMany(mappedBy = "building", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @Where(clause = "status = 'A'")
    private List<Project> projects;

    public Building(Long id) {
        this.id = id;
    }
}
