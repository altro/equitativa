package com.equitativa.testwork.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.equitativa.testwork.model.hibernate.StatusConverter;
import com.equitativa.testwork.model.hibernate.UserRoleConverter;
import com.equitativa.testwork.util.StringUtil;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "users")
@Entity
public class User {

    @Id
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    private Long id;
    @Convert(converter = StatusConverter.class)
    @Column(nullable = false)
    @NotNull
    private Status status;
    @Convert(converter = UserRoleConverter.class)
    @Column(nullable = false)
    @NotNull
    private UserRole role;
    @Column(nullable = false)
    @NotBlank
    private String login;
    @Column(nullable = false)
    @NotBlank
    private String password;
    @Column(nullable = false)
    @NotBlank
    private String salt;
    private String name;
    @Email(regexp = StringUtil.REGEXP_EMAIL, flags = Pattern.Flag.CASE_INSENSITIVE)
    private String email;

    @Column(nullable = false)
    @NotNull
    private LocalDateTime createTime;
    @Column(nullable = false)
    @NotNull
    private LocalDateTime updateTime;
    private LocalDateTime lastLoginTime;

    public User(Long id) {
        this.id = id;
    }
}
