package com.equitativa.testwork.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Getter
public enum UserRole {

    GUEST('g'),
    USER('u'),
    WORKER('w'),
    ADMIN('a'),
    ;

    public static final List<UserRole> ROLES = List.of(USER, WORKER, ADMIN);
    public static final Map<Character, UserRole> BY_CODE;

    static {

        var byCode = new HashMap<Character, UserRole>();
        for (var value : values()) {
            byCode.put(value.code, value);
        }

        BY_CODE = Map.copyOf(byCode);
    }

    private final Character code;

    public static UserRole byCode(char code) {
        var value = BY_CODE.get(code);
        if (value == null) {
            log.error("Unknown code: {}", code);
            return GUEST;
        }
        return value;
    }
}
