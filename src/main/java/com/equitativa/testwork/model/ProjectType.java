package com.equitativa.testwork.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.equitativa.testwork.model.hibernate.StatusConverter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "project_types")
@Entity
public class ProjectType {

    @Id
    @SequenceGenerator(name = "project_types_id_seq", sequenceName = "project_types_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_types_id_seq")
    private Long id;
    @Convert(converter = StatusConverter.class)
    @Column(nullable = false)
    @NotNull
    private Status status;
    @Column(nullable = false)
    @NotBlank
    private String title;

    public ProjectType(Long id) {
        this.id = id;
    }
}
