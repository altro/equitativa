package com.equitativa.testwork.model;

public enum ProjectState {

    NEW,
    ASSIGNED,
    IN_PROGRESS,
    COMPLETED,
}
