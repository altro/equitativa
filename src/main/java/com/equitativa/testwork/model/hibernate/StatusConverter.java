package com.equitativa.testwork.model.hibernate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.Status;

@Slf4j
@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, Character> {

    @Override
    public Character convertToDatabaseColumn(Status status) {
        if (status == null) {
            log.error("Status is null!!!", new Exception());
            return Status.REMOVED.getCode();
        }
        return status.getCode();
    }

    @Override
    public Status convertToEntityAttribute(Character statusCode) {
        return Status.byCode(statusCode);
    }
}
