package com.equitativa.testwork.model.hibernate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.model.UserRole;

@Slf4j
@Converter(autoApply = true)
public class UserRoleConverter implements AttributeConverter<UserRole, Character> {

    @Override
    public Character convertToDatabaseColumn(UserRole userRole) {
        if (userRole == null) {
            log.error("Status is null!!!", new Exception());
            return UserRole.GUEST.getCode();
        }
        return userRole.getCode();
    }

    @Override
    public UserRole convertToEntityAttribute(Character statusCode) {
        return UserRole.byCode(statusCode);
    }
}
