package com.equitativa.testwork.dao;

import java.util.List;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.model.ProjectType;
import com.equitativa.testwork.model.Status;

public class ProjectTypeDao extends BaseDao {

    private static ProjectTypeDao instance;

    public ProjectTypeDao(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public static ProjectTypeDao get() {
        if (instance == null) {
            instance = new ProjectTypeDao(HibernateContext.get());
        }

        return instance;
    }

    public List<ProjectType> find(int first, int count) {
        return getSession()
                .createQuery("from ProjectType where status = :status", ProjectType.class)
                .setFirstResult(first)
                .setMaxResults(count)
                .setParameter("status", Status.ACTIVE)
                .list();
    }

}
