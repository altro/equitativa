package com.equitativa.testwork.dao;

import java.util.List;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.model.Status;
import com.equitativa.testwork.model.User;
import com.equitativa.testwork.model.UserRole;

public class UserDao extends BaseDao {

    private static UserDao instance;

    public UserDao(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public static UserDao get() {
        if (instance == null) {
            instance = new UserDao(HibernateContext.get());
        }

        return instance;
    }

    public List<User> getByRole(UserRole role) {
        return getSession()
                .createQuery("from User where role = :role and status = :status", User.class)
                .setParameter("role", role)
                .setParameter("status", Status.ACTIVE)
                .list();
    }

    public User getByLogin(String login) {
        return getSession()
                .createQuery("from User where lower(login) = :login and status = :status", User.class)
                .setParameter("login", login)
                .setParameter("status", Status.ACTIVE)
                .uniqueResult();
    }

}
