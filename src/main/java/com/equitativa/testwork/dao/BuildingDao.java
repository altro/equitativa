package com.equitativa.testwork.dao;

import java.util.List;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.model.Building;
import com.equitativa.testwork.model.Status;

public class BuildingDao extends BaseDao {

    private static BuildingDao instance;

    public BuildingDao(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public static BuildingDao get() {
        if (instance == null) {
            instance = new BuildingDao(HibernateContext.get());
        }

        return instance;
    }

    public int getCount() {
        Number count = getSession()
                .createQuery("select count(id) from Building where status = :status", Number.class)
                .setParameter("status", Status.ACTIVE)
                .uniqueResult();
        return count != null ? count.intValue() : 0;
    }

    public Building getByCode(String code) {
        return getSession()
                .createQuery("from Building b where code = :code", Building.class)
                .setParameter("code", code.toLowerCase())
                .uniqueResult();
    }

    public List<Building> find(int first, int count) {
        return getSession()
                .createQuery("from Building b where status = :status", Building.class)
                .setParameter("status", Status.ACTIVE)
                .setFirstResult(first)
                .setMaxResults(count)
                .list();
    }

}
