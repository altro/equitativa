package com.equitativa.testwork.dao;

import java.io.Serializable;

import org.hibernate.Session;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.equitativa.testwork.dao.hibernate.HibernateContext;

@RequiredArgsConstructor
@Slf4j
public class BaseDao {

    @Getter
    protected final HibernateContext hibernateContext;

    public <T> T get(Class<? extends T> clazz, Serializable id) {
        return getSession().get(clazz, id);
    }

    public <T> Long save(T object) {
        return (Long) getSession().save(object);
    }

    public <T> void delete(T object) {
        getSession().delete(object);
    }

    public Session getSession() {
        return hibernateContext.getSession();
    }

    public void flushSession() {
        hibernateContext.flushSession();
    }

}
