package com.equitativa.testwork.dao.hibernate;

import javax.persistence.Persistence;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class HibernateContext {

    private static HibernateContext instance;

    private final ThreadLocal<Session> session = new ThreadLocal<>();
    private final ThreadLocal<Boolean> flushDisabled = new ThreadLocal<>();

    private final SessionFactory sessionFactory;

    public static HibernateContext get() {
        if (instance == null) {
            var entityManagerFactory = Persistence.createEntityManagerFactory("com.equitativa.testwork.model");
            var sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
            instance = new HibernateContext(sessionFactory);
        }

        return instance;
    }

    public Session getSession(boolean createNew) {
        Session s = session.get();
        if (s == null && createNew) {
            return getSession();
        }
        return s;
    }

    public Session getSession() {
        Session s = session.get();
        if (s != null && s.isOpen()) {
            return s;
        }
        if (sessionFactory == null || sessionFactory.isClosed()) {
            throw new RuntimeException("Session factory is not initialized or closed.");
        }
        log.debug("Creating session");
        s = sessionFactory.openSession();
        s.setHibernateFlushMode(FlushMode.COMMIT);
        s.setCacheMode(CacheMode.NORMAL);
        session.set(s);
        s.beginTransaction();
        return s;
    }

    public void closeSession(boolean commit) {
        Session s = session.get();
        if (s == null) {
            return;
        }
        log.debug("Closing session");
        try {
            if (s.getTransaction() != null && s.getTransaction().isActive()) {
                if (commit) {
                    doCommit(s);
                } else {
                    s.getTransaction().rollback();
                }
            }
        } finally {
            try {
                s.close();
            } finally {
                session.set(null);
            }
        }
    }

    public void commitSession() {
        Session s = session.get();
        if (s == null) {
            return;
        }
        if (s.getTransaction() != null && s.getTransaction().isActive()) {
            Boolean disabled = flushDisabled.get();
            if (disabled != null && disabled) {
                // don't flush if it is disabled
                return;
            }

            doCommit(s);
            s.beginTransaction();
        }
    }

    private void doCommit(Session s) {
        Transaction transaction = s.getTransaction();
        try {
            transaction.commit();
        } catch (Exception e) {
            log.debug("Error with transaction commit. Transaction rolled back");
            try {
                transaction.rollback();
            } catch (Exception e1) {
                log.error("Transaction roll back error", e1);
            }
        }
    }

    public void flushSession() {
        Session s = session.get();
        if (s == null) {
            return;
        }

        Boolean disabled = flushDisabled.get();
        if (disabled != null && disabled) {
            // don't flush if it is disabled
            return;
        }

        s.flush();
    }

    public void destroy() {
        Session s = session.get();
        if (s != null) {
            s.close();
        }
        sessionFactory.close();
    }

    public void disableFlush() {
        flushDisabled.set(true);
    }

    public void enableFlush() {
        flushDisabled.set(false);
    }
}
