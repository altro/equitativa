package com.equitativa.testwork.dao;

import java.util.List;

import com.equitativa.testwork.dao.hibernate.HibernateContext;
import com.equitativa.testwork.model.Project;
import com.equitativa.testwork.model.Status;

public class ProjectDao extends BaseDao {

    private static ProjectDao instance;

    public ProjectDao(HibernateContext hibernateContext) {
        super(hibernateContext);
    }

    public static ProjectDao get() {
        if (instance == null) {
            instance = new ProjectDao(HibernateContext.get());
        }

        return instance;
    }

    public List<Project> findByBuildingId(long buildingId, int first, int count) {
        return getSession()
                .createQuery("from Project p where p.building.id = :buildingId and p.status = :status", Project.class)
                .setParameter("buildingId", buildingId)
                .setParameter("status", Status.ACTIVE)
                .setFirstResult(first)
                .setMaxResults(count)
                .list();
    }

    public int getCountByBuildingId(long buildingId) {
        Number count = getSession()
                .createQuery("select count(id) from Project where status = :status and building.id = :buildingId", Number.class)
                .setParameter("buildingId", buildingId)
                .setParameter("status", Status.ACTIVE)
                .uniqueResult();
        return count != null ? count.intValue() : 0;
    }

    public List<Project> findByBuildingIdAndAssignedUserId(long buildingId, long assignedUserId, int first, int count) {
        return getSession()
                .createQuery("from Project p where p.building.id = :buildingId and p.assignedUser.id = :assignedUserId and p.status = :status", Project.class)
                .setParameter("buildingId", buildingId)
                .setParameter("assignedUserId", assignedUserId)
                .setParameter("status", Status.ACTIVE)
                .setFirstResult(first)
                .setMaxResults(count)
                .list();
    }

    public int getCountByBuildingIdAndAssignedUserId(long buildingId, long assignedUserId) {
        Number count = getSession()
                .createQuery("select count(id) from Project where status = :status and building.id = :buildingId and assignedUser.id = :assignedUserId", Number.class)
                .setParameter("buildingId", buildingId)
                .setParameter("assignedUserId", assignedUserId)
                .setParameter("status", Status.ACTIVE)
                .uniqueResult();
        return count != null ? count.intValue() : 0;
    }
}
